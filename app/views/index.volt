<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1,opera=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <title>Arena</title>

        <!--     Fonts     -->
        <link rel="stylesheet" href="<?= $this->url->getBaseUri();?>public/css/arena.css">
        <link rel="stylesheet" href="<?= $this->url->getBaseUri();?>public/css/status.css">

        <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link rel="stylesheet" href="<?= $this->url->getBaseUri();?>public/css/media-queries.css">
        <link rel="stylesheet" href="<?= $this->url->getBaseUri();?>public/css/carousel.css">
        <link href="<?= $this->url->getBaseUri();?>public/css/animate.css" rel="stylesheet" />

    </head>

    <body class="App-container">


        <nav class="navbar navbar-transparent niveau-100  justify-content-between fixed-top" >
            <span class=""  onclick="">  
                <div id="nav-icon1">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </span> <span class="logostyle"  onclick="openNav()">  
                Arena
            </span>
            <span class="">  
                <div id="nav-icon1">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </span>
        </nav>


        <!-- The overlay -->
        <div id="myNav" class="overlay">

            <!-- Button to close the overlay navigation -->
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

            <!-- Overlay content -->
            <div class="overlay-content">
                <div class="">


                    <div class="container-fluid">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner row w-100 mx-auto  wow fadeInRight" role="listbox">
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 img-50 active">
                                    <span class="author img-fluid d-block text-center">
                                        <a href="#"><img alt="img1" class="img-50 size-3 img-fluid mx-auto d-block" src="https://www.cocacola.fr/content/dam/GO/CokeZone/Common/global/logo/logodesktop/coca-cola-logo-260x260.png"></a>
                                        <h6 class="white">CocaCola</h6> 
                                    </span>
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <span class="author img-fluid  d-block text-center">
                                        <a href="#"><img alt="img2" class="img-50 size-3 img-fluid mx-auto d-block" src="<?= $this->url->getBaseUri(); ?>public/img/new_logo.png"></a>
                                        <h6 class="white">CocaCola</h6> 
                                    </span>
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <span class="author img-fluid d-block text-center">
                                        <a href="#"><img alt="img3" class="img-50 size-3" src="<?= $this->url->getBaseUri(); ?>public/img/new_logo.png"></a>
                                        <h6 class="white">CocaCola</h6> 
                                    </span>
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <span class="author img-fluid mx-auto d-block text-center">
                                        <a href="#"><img alt="img4" class="img-50 size-3" src="<?= $this->url->getBaseUri(); ?>public/img/new_logo.png"></a>
                                        <h6 class="white">CocaCola</h6> 
                                    </span>
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <span class="author img-fluid mx-auto d-block text-center">
                                        <a href="#"><img alt="img4" class="img-50 size-3" src="<?= $this->url->getBaseUri(); ?>public/img/new_logo.png"></a>
                                        <h6 class="white">CocaCola</h6> 
                                    </span>
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <span class="author img-fluid mx-auto d-block text-center">
                                        <a href="#"><img alt="img4" class="img-50 size-3" src="<?= $this->url->getBaseUri(); ?>public/img/new_logo.png"></a>
                                        <h6 class="white">CocaCola</h6> 
                                    </span>
                                </div>

                            </div>
                            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>



        <div class="">
            {{ content() }}
        </div>


        <footer class="footer">      

            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            <script src="<?= $this->url->getBaseUri();?>public/js/jquery-3.4.1.js" type="text/javascript"></script>        
            <script src="<?= $this->url->getBaseUri();?>public/js/arena.js" type="text/javascript"></script>
            <script src="<?= $this->url->getBaseUri();?>public/js/carousel.js" type="text/javascript"></script>
            <script src="<?= $this->url->getBaseUri();?>public/js/wow.min.js" type="text/javascript"></script>
            <script>

                new WOW().init();

                $(document).ready(function () {
                    $('#nav-icon1').click(function () {
                        $(this).toggleClass('open');
                    });
                });

            </script>

            <div class="footer">
                &copy; Développé avec  <i class="fa fa-heart heart"></i>  par Cadet et Kyriel</a>.
            </div>

        </footer>


    </body>


</html>
